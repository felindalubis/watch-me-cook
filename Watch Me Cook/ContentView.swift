//
//  ContentView.swift
//  Watch Me Cook
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
