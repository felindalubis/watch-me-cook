//
//  Watch_Me_CookApp.swift
//  Watch Me Cook
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import SwiftUI

@main
struct Watch_Me_CookApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
