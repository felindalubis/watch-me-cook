//
//  CheckpointView.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 23/08/21.
//

import SwiftUI

struct CheckpointView: View {
    @State var selectedRecipe: RecipeModel
    var index: Int
    var body: some View {
        ScrollView {
            VStack {
                ZStack {
                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                        .fill(Color.gray)
//                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                    VStack {
                        Image(systemName: "heart.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .padding(.top)
                            .frame(width: 60, height: 60, alignment: .center)
                        Text(selectedRecipe.steps[index].instruction.uppercased())
                            .padding()
                            .font(.title3)
                    }
                }
                if let ingredients = selectedRecipe.steps[index].ingredients {
                    ForEach(ingredients.indices) { i in
                        Text(ingredients[i])
                            .padding(.vertical, 3)
                    }
                }
            }
        }
    }
}

struct CheckpointView_Previews: PreviewProvider {
    static var previews: some View {
        CheckpointView(selectedRecipe: RecipeManager().recipes[0], index: 0)
    }
}
