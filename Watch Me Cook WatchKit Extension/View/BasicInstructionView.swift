//
//  BasicInstructionView.swift
//  Watch Me Cook WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import SwiftUI

struct BasicInstructionView: View {
    @State var selectedRecipe: RecipeModel
    var index: Int
    var body: some View {
        VStack(alignment: .center) {
            Text(selectedRecipe.steps[index].instruction.uppercased())
                .font(.title3)
                .padding()
                .multilineTextAlignment(.center)
        }
    }
}

struct BasicInstructionView_Previews: PreviewProvider {
    static var previews: some View {
        BasicInstructionView(selectedRecipe: RecipeManager().recipes[0], index: 0)
    }
}
