//
//  TimerView.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 23/08/21.
//

import SwiftUI

struct TimerView: View {
    var selectedRecipe: RecipeModel
    var index: Int
    @ObservedObject var timerVM: TimerViewModel
    @Binding var isActive: Bool
    let timer = Timer.publish(every: 1, tolerance: 0.5, on: .main, in: .common).autoconnect()
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .trim(from: 0.55, to: 0.95)
                    .stroke(Color.gray.opacity(0.2), style: StrokeStyle(lineWidth: 8, lineCap: .round))
                    .padding()
                Circle()
                    // 0.05 to 0.45
                    .trim(from: 0.05, to: CGFloat(timerVM.progress))
                    .stroke(Color.blue, style: StrokeStyle(lineWidth: 8, lineCap: .round))
                    .rotationEffect(.degrees(-180))
                    .animation(.easeInOut)
                    .padding()
                VStack {
                    Text(isActive ? "\(timerVM.timerStr)" : "Done!".uppercased())
                        .font(.title3)
                    Text(selectedRecipe.steps[index].instruction)
                        .font(.title3)
                        .padding()
                        .multilineTextAlignment(.center)
                }
                .offset(y: 10)
            }
        }
        .onReceive(timer, perform: { _ in
            guard isActive else {return}
            if timerVM.timeRemaining > 0 {
                timerVM.updateProgressBar()
            } else {
                WKInterfaceDevice().play(.success)
                isActive = false
                self.timer.upstream.connect().cancel()
            }
        })
        .onAppear(perform: {
            isActive = true
            timerVM.cookTime = selectedRecipe.steps[index].timer ?? 0
            timerVM.timeRemaining = timerVM.cookTime
            timerVM.updateTimerLabel()
        })
        .onDisappear(perform: {
            timerVM.resetTimer()
        })
    }
    

    
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView(selectedRecipe: RecipeManager().recipes[0], index: 0, timerVM: TimerViewModel(), isActive: .constant(true))
    }
}
