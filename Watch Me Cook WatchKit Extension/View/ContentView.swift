//
//  ContentView.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 20/08/21.
//

import SwiftUI

struct ContentView: View {
    var recipeManager = RecipeManager()
    @ObservedObject var timerVM: TimerViewModel
    var body: some View {
        ScrollView {
            ZStack {
                VStack {
                    Text("Watch Me Cook")
                        .padding()
                    ForEach(recipeManager.recipes.indices) { i in
                        ZStack {
                            NavigationLink(recipeManager.recipes[i].title, destination: InstructionView(timerVM: timerVM, selectedRecipe: recipeManager.recipes[i]))
                        }
                    }
                }
            }
            .navigationTitle("Cook")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(timerVM: TimerViewModel())
    }
}
