//
//  InstructionView.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 23/08/21.
//

import SwiftUI
import AVFoundation

struct InstructionView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var timerVM: TimerViewModel
    @State var selectedRecipe: RecipeModel
    @State var index:Int = 0
    @State var isActive = false
    @State var utterance: AVSpeechUtterance!
    let synthesizer = AVSpeechSynthesizer()
    
    var body: some View {
        ZStack {
            Color.black
            VStack {
                // Instruction available
                if index < selectedRecipe.steps.count {
                    // Instruction with no timer
                    if selectedRecipe.steps[index].timer == nil {
                        // Checkpoint instruction
                        if selectedRecipe.steps[index].isCheckpoint {
                            CheckpointView(selectedRecipe: selectedRecipe, index: index)
                            // Basic instruction
                        } else {
                            BasicInstructionView(selectedRecipe: selectedRecipe, index: index)
                        }
                        // Instruction with timer
                    } else {
                        TimerView(selectedRecipe: selectedRecipe, index: index, timerVM: timerVM, isActive: $isActive)
                    }
                }
                // Instruction finished
                else {
                    Text("🏆")
                        .font(.title)
                        .padding()
                    Text("Congratulations!".uppercased())
                        .foregroundColor(.orange)
                        .bold()
                    Text("Your meal is ready")
                    Button("Done") {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                    .padding()
                }
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .onTapGesture {
            if !isActive {
                nextStep()
            }
        }
        .onAppear(perform: {
            textToSpeech(text: getStringForSpeech())
        })
        .navigationTitle(self.index > 0 ? "\(self.index)/\(selectedRecipe.steps.count)" : "")
    }
    
    func nextStep() {
        if index < selectedRecipe.steps.count && selectedRecipe.steps.count > 1 {
            index += 1
            textToSpeech(text: getStringForSpeech())
        }
    }
    
    func textToSpeech(text: String) {
        synthesizer.stopSpeaking(at: .immediate)
        utterance = AVSpeechUtterance(string: text)
        
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.3
        
        synthesizer.speak(utterance)
    }
    
    func getStringForSpeech() -> String {
        var str: String = ""
        if index < selectedRecipe.steps.count {
            let step = selectedRecipe.steps[index]
            let arrStr = step.ingredients?.joined(separator: ", ")
            let ingredientsSpeech = ". You will need: \(arrStr ?? "")"
            str += step.instruction
            
            // If it's a checkpoint add ingredients list
            if step.isCheckpoint {
                str += "\(step.ingredients != nil ? ingredientsSpeech : "")"
            }
            
            // If instruction has timer
            if let timer = step.timer {
                if timer >= 60 {
                    let minute = String(timer/60)
                    let second = timer%60
                    str += "For \(minute) minutes"
                    if second != 0 {
                        str += "and \(second) seconds"
                    }
                } else {
                    str += "For \(timer) seconds"
                }
            }
        } else {
            str = "Congratulations! Your meal is ready"
        }
        print(str)
        return str
    }
}

struct InstructionView_Previews: PreviewProvider {
    static var previews: some View {
        InstructionView(timerVM: TimerViewModel(), selectedRecipe: RecipeModel(title: "Hello", steps: [StepModel(instruction: "Hello", timer: nil)]))
    }
}
