//
//  RecipeManager.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 23/08/21.
//

import Foundation

struct RecipeManager {
    var recipes = [
        RecipeModel(title: "Sirloin Steak", steps: [
            StepModel(instruction: "Tap screen when you're done", timer: nil),
            StepModel(isCheckpoint: true, ingredients: ["Steak","Salt", "Pepper"], instruction: "Seasoning" , timer: nil),
            StepModel(instruction: "Add a pinch of salt", timer: nil),
            StepModel(instruction: "Add a pinch of pepper", timer: nil),
            StepModel(isCheckpoint: true, ingredients: ["Pan", "Oil"], instruction: "Pan Preparation", timer: nil),
            StepModel(instruction: "heat the cast iron pan until hot", timer: nil),
            StepModel(instruction: "Add steaks to the skillet", timer: nil),
            StepModel(instruction: "Sear the steaks", timer: 10),
            StepModel(instruction: "Flip the steak to other side", timer: nil),
            StepModel(instruction: "Sear the steaks", timer: 10),
            StepModel(isCheckpoint: true, ingredients: ["Butter", "Garlic", "Rosemary"], instruction: "Basting", timer: nil),
            StepModel(instruction: "Add 2 tbs of butter", timer: nil),
            StepModel(instruction: "Add 2 cloves of garlic", timer: nil),
            StepModel(instruction: "add rosemary", timer: nil),
            StepModel(instruction: "Spoon butter over the steak", timer: 6),
            StepModel(isCheckpoint: true, ingredients: ["Knive", "Cutting board", "Plate"], instruction: "Resting", timer: nil),
            StepModel(instruction: "Transfer steak to a cutting board", timer: nil),
            StepModel(instruction: "Rest the steak", timer: 6),
            StepModel(instruction: "slicing into strips to serve", timer: nil),
            StepModel(instruction: "Spoon extra butter sauce over sliced steak to serve", timer: nil)
        ]),
        RecipeModel(title: "Indomie", steps: [
            StepModel(instruction: "Tap screen when you're done", timer: nil),
            StepModel(isCheckpoint: true, ingredients: ["Indomie", "Pan", "2 Glass of Water"],  instruction: "Cook Noodle", timer: nil),
            StepModel(instruction: "Pour water into the pan"),
            StepModel(instruction: "Boil the water", timer: 15),
            StepModel(instruction: "Put noodles into boiling water"),
            StepModel(instruction: "Cook the noodles", timer: 15),
            StepModel(instruction: "Drain the noodles"),
            StepModel(isCheckpoint: true, ingredients: ["Indomie seasonings", "Bowl"], instruction: "Seasonings"),
            StepModel(instruction: "Pour the seasonings into bowl"),
            StepModel(instruction: "Put the noodle into bowl"),
            StepModel(instruction: "Stir until seasonings and noodle mixed")
        ]),
        RecipeModel(title: "Salad", steps: [
            StepModel(instruction: "Coming soon!", timer: nil)
        ]),
        RecipeModel(title: "Boiled Egg", steps: [
            StepModel(instruction: "Coming soon!", timer: nil)
        ]),
        RecipeModel(title: "Fried Rice", steps: [
            StepModel(instruction: "Coming soon!", timer: nil)
        ])
        
    ]
}
