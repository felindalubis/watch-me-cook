//
//  RecipeModel.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 20/08/21.
//

import Foundation

struct RecipeModel {
    var title: String
    var steps: [StepModel]
}

struct StepModel {
    var isCheckpoint: Bool = false
    var ingredients: [String]?
    var instruction: String
    var timer: Int?
}

