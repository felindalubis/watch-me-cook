//
//  TimerViewModel.swift
//  Watch Me Cook - SwiftUI WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import Foundation

class TimerViewModel: ObservableObject {
    @Published var cookTime: Int = 0
    @Published var timeRemaining: Int = 0
    @Published var progress: Float = 0.05
    @Published var timerStr = "00:00"
    @Published var leftTime: Date?
    
    func updateTimerLabel(){
        let minutes = (timeRemaining/60)
        let seconds = (timeRemaining%60)
        let minutesStr = String(minutes < 10 ? "0\(minutes)" : "\(minutes)")
        let secondStr =  String(seconds < 10 ? "0\(seconds)" : "\(seconds)")
        
        timerStr = minutesStr+":"+secondStr
    }
    
    func updateProgressBar() {
        timeRemaining -= 1
        progress = 0.05 + (1-(Float(timeRemaining)/Float(cookTime)))*0.4

        updateTimerLabel()
    }
    
    func resetTimer() {
        progress = 0.05
    }

}
