//
//  Watch_Me_CookApp.swift
//  Watch Me Cook WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import SwiftUI

@main
struct Watch_Me_CookApp: App {
    @StateObject var timerVM = TimerViewModel()
    @Environment(\.scenePhase) var scenes
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView(timerVM: timerVM)
            }
        }
        .onChange(of: scenes) { scene in
            if scene == .inactive {
                timerVM.leftTime = Date()
            } else if scene == .active && timerVM.timeRemaining != 0, let date = timerVM.leftTime {
                let diff = Date().timeIntervalSince(date)
                let currentTime = timerVM.timeRemaining - Int(diff)
                if currentTime >= 0 {
                    withAnimation {
                        timerVM.timeRemaining = currentTime
                    }
                }
            }
        }
    }
}
